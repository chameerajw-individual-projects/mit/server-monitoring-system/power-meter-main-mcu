const int sample_count = 100;
const int delay_size = 200;  //us
int value_array[100] = { 0 };

void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
}

void loop() {

  for (int i = 0; i < sample_count; i++) {
    value_array[i] = analogRead(A3);
    delayMicroseconds(delay_size);
  }
  int maxVal = 0;
  int minVal = 10000;
  for (int i = 0; i < sample_count; i++) {
    Serial.print(value_array[i]);
    Serial.print(",");
    maxVal = max(value_array[i],maxVal);
    minVal = min(value_array[i],minVal);
    delay(100);
  }

  Serial.println("");

  Serial.print("MAX: ");
  Serial.println(maxVal);
  Serial.print("MIN: ");
  Serial.println(minVal);


  Serial.println("NEW LINE");
  delay(5000);

  // put your main code here, to run repeatedly:
}
