#ifndef GSM_H
#define GSM_H

#include "common_def.h"

#define SIM800_RX 2  //sim800 TX pin to pin 2. Arduino RX
#define SIM800_TX 3  //sim800 RX pin to pin 3. Arduino TX
#define SIM800_RST 4

#define MAX_GSM_INIT_ATTEMPTS 5

//SoftwareSerial serialGsm(SIM800_RX, SIM800_TX);  // RX, TX
#define serialGsm Serial2

void GSMInit(void);
void printGSM(void);
void clearGSMBuffer(void);
void resetGSM(void);
void GSMGetMessageDecoded(gsmParams_s* gsm_param);
void gsmDecoder(String new_msg, gsmParams_s* gsm_param);
void sendSensorValuesGSM(sensorVal_s* sensor_values, String phone_number);
void sendSetConfirmation(String string_param, String phone_number);
bool sendSensorValuesGSM_local(char* cfg_number, sensorVal_s* sensor_values);
bool sendThresholdsGSM_local(char* cfg_number, sensorThreshold_s* threshold);

void GSMInit(void) {
  pinMode(SIM800_RST, OUTPUT);
  digitalWrite(SIM800_RST, HIGH);

  resetGSM();
  delay(5000);
  serialGsm.begin(9600);
  serialGsm.flush();;
  //serialGsm.listen();

  uint8_t retry_count = MAX_GSM_INIT_ATTEMPTS;
  while (retry_count > 0) {
    Serial.print('.');
    serialGsm.println("AT");
    ////serialGsm.listen();
    delay(1000);

    if ((10 >= serialGsm.available()) && (serialGsm.available() > 0)) {
      String income;
      income = serialGsm.readString();
      
      // Serial.print(F("Initialize income: "));
      // Serial.println(income);

      int8_t index_of_O = income.indexOf('O');
      if (index_of_O >= 0) {
        Serial.println("\t GSM 1 INITIALIZED..");
        break;
      }
    }
    retry_count--;
    delay(1000);
  }


  serialGsm.println("AT+CFUN?");
  delay(1000);
  clearGSMBuffer();

  serialGsm.println("AT+CMGF=1");  // Configuring TEXT mode
  delay(100);
  clearGSMBuffer();

  serialGsm.println("AT+CNMI=2,1,0,0,0");  // Decides how newly arrived SMS messages should be handled
  delay(100);
  clearGSMBuffer();

  serialGsm.println("AT+CLIP=1");  // Decides how newly arrived SMS messages should be handled
  delay(100);
  clearGSMBuffer();

  delay(20000);  //wait till SMS Ready and CALL Ready returns
  clearGSMBuffer();

  serialGsm.println("AT+CMGD=1,4"); //delete entire sms buffer inside the device
  delay(100);
  clearGSMBuffer();
}

void printGSM(void) {
  while (serialGsm.available() > 0) {
    Serial.write(serialGsm.read());
  }
}

void clearGSMBuffer(void) {
  while (serialGsm.available() > 0) {
    serialGsm.read();
  }
}

void resetGSM(void) {
  digitalWrite(SIM800_RST, LOW);
  delay(500);
  digitalWrite(SIM800_RST, HIGH);
  delay(5000);
}

void GSMGetMessageDecoded(gsmParams_s* gsm_param) {
  //temp=13.56
  //hum=70.0
  //bat=3.75
  //ups
  //dashboard=2 //how many times per minute
  //wifi=ssid,password
  //get_thresh
  //get_sensor

  /*Income complete: 
    +CMT: "+94707659129","","23/03/17,23:40:17+22"
    Ups 

    Usually the complete message shows like above. To detect the command
      1. Find the start of the command (find the index of '\n' after 5 characters, and add 1)
      2. Check for '=' presence and substring upto it
      3. otherwise substring upto the final "\n"
*/

  gsm_param->gsm_command = GSM_EMPTY;
  gsm_param->float_param = 0.0;
  gsm_param->string_param = "";

  //serialGsm.listen();
  //clearGSMBuffer();
  ////serialGsm.flush();;
  
  serialGsm.println("AT+CMGR=1");
  ////serialGsm.listen();
  //delay(2000);
  if (serialGsm.available()) {
    String income_msg = serialGsm.readString();
    // delay(1000);
    
    // clearGSMBuffer();
    // //serialGsm.flush();;

    // Serial.print(F("Income complete: "));
    // Serial.println(income_msg);

    int8_t index_of_nl = income_msg.indexOf('\n');
    String cmgr_response = income_msg.substring((index_of_nl + 1), (index_of_nl + 1 + 5));

    if (cmgr_response == "+CMGR") {
      //new message is available
      index_of_nl = income_msg.indexOf('\n', 60);
      
      cmgr_response = income_msg.substring((index_of_nl + 1));

      Serial.print(F("Income gsmDecoder: "));
      Serial.println(cmgr_response);

      int8_t index_of_cmd_start = cmgr_response.indexOf('\n') + 1;
      int8_t index_of_cmd_end = cmgr_response.indexOf('=');

      String phone_number = cmgr_response.substring(19, 31);  //+94712961909

      gsm_param->phone_number = phone_number;

      //Serial.print(F("phone_number: "));
      //Serial.println(phone_number);

      String cmd_received = "";
      String value_received_str = "";
      float value_received_float = 0.0;

      if (index_of_cmd_end < 0)  //if '=' is not present
      {
        cmd_received = "INVALID";
      } else     //if '=' is present                                                               
      {
        cmd_received = cmgr_response.substring(index_of_cmd_start, index_of_cmd_end);
        int8_t index_of_value_end = cmgr_response.indexOf('\n', index_of_cmd_start) - 1;
        value_received_str = cmgr_response.substring(index_of_cmd_end + 1, index_of_value_end);
      }

      if (cmd_received == "temp") {
        gsm_param->gsm_command = GSM_SET_TEMPERATURE_THRESHOLD;
        value_received_float = (float)(value_received_str.toFloat());
        gsm_param->float_param = value_received_float;
      } else if (cmd_received == "hum") {
        gsm_param->gsm_command = GSM_SET_HUMIDITY_THRESHOLD;
        value_received_float = (float)(value_received_str.toFloat());
        gsm_param->float_param = value_received_float;
      } else if (cmd_received == "bat") {
        gsm_param->gsm_command = GSM_SET_BATTERY_THRESHOLD;
        value_received_float = (float)(value_received_str.toFloat()); //cast temporary as float. later will be treated as an Int
        gsm_param->float_param = value_received_float;
      } else if (cmd_received == "ups") {
        gsm_param->gsm_command = GSM_RESET_UPS;
      } else if (cmd_received == "dashboard") {
        gsm_param->gsm_command = GSM_SET_DASHBOARD_FREQ;
        value_received_float = (float)(value_received_str.toFloat());
        gsm_param->float_param = value_received_float;
      } else if (cmd_received == "wifi") {
        gsm_param->gsm_command = GSM_SET_WIFI;
        gsm_param->string_param = value_received_str;
      } else if (cmd_received == "get_thresh") {
        gsm_param->gsm_command = GSM_GET_THRESHOLD;
      } else if (cmd_received == "get_sensor") {
        gsm_param->gsm_command = GSM_GET_SENSOR_STATUS;
      } else if (cmd_received == "reinit") {
        gsm_param->gsm_command = GSM_REINIT;
      }else {
        Serial.println(F("UNDEFINED COMMAND"));
        gsm_param->gsm_command = GSM_UNDEFINED;
      }


      Serial.println(F("************************"));
      Serial.print(F("gsm_param->phone_number: "));
      Serial.println(gsm_param->phone_number);
      Serial.print(F("gsm_param->gsm_command: "));
      Serial.println(gsm_param->gsm_command);
      Serial.print(F("gsm_param->string_param: "));
      Serial.println(gsm_param->string_param);
      Serial.print(F("gsm_param->float_param: "));
      Serial.println(gsm_param->float_param);
      Serial.println(F("************************"));

      delay(100);

      serialGsm.println("AT+CMGD=1,4");  //delete entire sms buffer inside the device
      delay(100);
      clearGSMBuffer();

      return;  // no need to proceed if sms received sucessfully
    } else {
      //no new message
    }
  }
}

// void gsmDecoder(String new_msg, gsmParams_s* gsm_param)
// {
//   Serial.print(F("Income gsmDecoder: "));
//   Serial.println(new_msg);

//   int8_t index_of_cmd_start = new_msg.indexOf('\n') + 1;
//   int8_t index_of_cmd_end = new_msg.indexOf('=');

//   String phone_number = new_msg.substring(21, 33);  //+94712961909

//   gsm_param->phone_number = phone_number;

//   //Serial.print(F("phone_number: "));
//   //Serial.println(phone_number);

//   String cmd_received = "";
//   String value_received_str = "";
//   float value_received_float = 0.0;

//   if (index_of_cmd_end < 0)  //if '=' is not present
//   {
//     cmd_received = "INVALID";
//   } else     //if '=' is present                                                               
//   {
//     cmd_received = new_msg.substring(index_of_cmd_start, index_of_cmd_end);
//     int8_t index_of_value_end = new_msg.indexOf('\n', index_of_cmd_start) - 1;
//     value_received_str = new_msg.substring(index_of_cmd_end + 1, index_of_value_end);
//   }

//   if (cmd_received == "temp") {
//     gsm_param->gsm_command = GSM_SET_TEMPERATURE_THRESHOLD;
//     value_received_float = (float)(value_received_str.toFloat());
//     gsm_param->float_param = value_received_float;
//   } else if (cmd_received == "hum") {
//     gsm_param->gsm_command = GSM_SET_HUMIDITY_THRESHOLD;
//     value_received_float = (float)(value_received_str.toFloat());
//     gsm_param->float_param = value_received_float;
//   } else if (cmd_received == "bat") {
//     gsm_param->gsm_command = GSM_SET_BATTERY_THRESHOLD;
//     value_received_float = (float)(value_received_str.toFloat()); //cast temporary as float. later will be treated as an Int
//     gsm_param->float_param = value_received_float;
//   } else if (cmd_received == "ups") {
//     gsm_param->gsm_command = GSM_RESET_UPS;
//   } else if (cmd_received == "dashboard") {
//     gsm_param->gsm_command = GSM_SET_DASHBOARD_FREQ;
//     value_received_float = (float)(value_received_str.toFloat());
//     gsm_param->float_param = value_received_float;
//   } else if (cmd_received == "wifi") {
//     gsm_param->gsm_command = GSM_SET_WIFI;
//     gsm_param->string_param = value_received_str;
//   } else if (cmd_received == "get_thresh") {
//     gsm_param->gsm_command = GSM_GET_THRESHOLD;
//   } else if (cmd_received == "get_sensor") {
//     gsm_param->gsm_command = GSM_GET_SENSOR_STATUS;
//   } else if (cmd_received == "reinit") {
//     gsm_param->gsm_command = GSM_REINIT;
//   }else {
//     Serial.println(F("UNDEFINED COMMAND"));
//     gsm_param->gsm_command = GSM_UNDEFINED;
//   }


//   Serial.println(F("************************"));
//   Serial.print(F("gsm_param->phone_number: "));
//   Serial.println(gsm_param->phone_number);
//   Serial.print(F("gsm_param->gsm_command: "));
//   Serial.println(gsm_param->gsm_command);
//   Serial.print(F("gsm_param->string_param: "));
//   Serial.println(gsm_param->string_param);
//   Serial.print(F("gsm_param->float_param: "));
//   Serial.println(gsm_param->float_param);
//   Serial.println(F("************************"));

//   delay(100);
// }

void sendThresholdsGSM(sensorThreshold_s* threshold, String phone_number) {
  char* cfg_number; 
  cfg_number = malloc(23 * sizeof(char));
  strcpy(cfg_number, "AT+CMGS=\"");
  strcat(cfg_number, phone_number.c_str());
  strcat(cfg_number, "\"\0");

  uint8_t failedCount = 0;
  uint8_t reinitCount = 0;

  do{
    bool isSendSuccess = false;
    isSendSuccess = sendThresholdsGSM_local(cfg_number, threshold);
    
    break;
    // if (isSendSuccess == false) {
    //     failedCount++;
    // }
    // else{
    //   //send sucess;
    //   break;
    // }
  
    // if(failedCount > 3)   
    // {
    //   GSMInit();
    //   reinitCount ++;
    // }

    // if(reinitCount > 1)
    // {
    //   //reinited more than once. still failed
    //   break;
    // }

  }while (1);  //end of while loop

  free(cfg_number);
}

bool sendThresholdsGSM_local(char* cfg_number, sensorThreshold_s* threshold)
{
do {
    Serial.print("Sending threshold values = ");
    
    //serialGsm.listen();
    clearGSMBuffer();
    ////serialGsm.flush();;

    serialGsm.println("AT");  //Once the handshake test is successful, it will back to OK
    delay(500);
    clearGSMBuffer();

    serialGsm.println(cfg_number);
    delay(500);

    /*String number_send_response = "ERROR";
    if (serialGsm.available()) {
      String full = serialGsm.readString();
      int index_of_nl = full.indexOf('\n');
      number_send_response = full.substring(abs(index_of_nl) + 1, abs(index_of_nl) + 1 + 5);  //get 5 characters after the NL
    }
    if (String("ERROR") == number_send_response) {
      //strings are equal. ERROR
      Serial.println("FAILED at CFG number");
      return false;
    }*/

    serialGsm.print("tempThresh: ");
    serialGsm.print(threshold->temperature_thresh, 2);  
    serialGsm.print(" C\nhumThresh: ");
    serialGsm.print(threshold->humidity_thresh, 2);  
    serialGsm.print(" %\nbatThresh: ");
    serialGsm.print(threshold->battery_thresh);  
    serialGsm.print(" %");

    clearGSMBuffer();

    serialGsm.write(26);  //do not change. it is the Ctrl+Z in the ascii table

    delay(10000);  //wait till output get cleared

    //serialGsm.listen();
    clearGSMBuffer();
    //serialGsm.flush();;

    /*String send_response = "ERROR";
    if (serialGsm.available()) {
      String full = serialGsm.readString();

      //Serial.print(F("Income full: "));
      //Serial.println(full);

      int index_of_plus = full.indexOf('+');
      send_response = full.substring(abs(index_of_plus), abs(index_of_plus) + 5);  //get 5 characters including +
    }
    if (String("+CMGS") != send_response) {
      //strings are not equal. ERROR
      Serial.println("FAILED at +CMGS");
      return false;
    }*/
    Serial.println("SUCCESS!!!");

  } while (0);
  return true;
}

void sendSensorValuesGSM(sensorVal_s* sensor_values, String phone_number) 
{
  char* cfg_number; 
  cfg_number = malloc(23 * sizeof(char));
  strcpy(cfg_number, "AT+CMGS=\"");
  strcat(cfg_number, phone_number.c_str());
  strcat(cfg_number, "\"\0");

  uint8_t failedCount = 0;
  uint8_t reinitCount = 0;

  do{
    bool isSendSuccess = false;
    isSendSuccess = sendSensorValuesGSM_local(cfg_number, sensor_values);
    
    break;
    // if (isSendSuccess == false) {
    //     failedCount++;
    // }
    // else{
    //   //send sucess;
    //   break;
    // }
  
    // if(failedCount > 3)   
    // {
    //   GSMInit();
    //   reinitCount ++;
    // }

    // if(reinitCount > 1)
    // {
    //   //reinited more than once. still failed
    //   break;
    // }

  }while (1);  //end of while loop

  free(cfg_number);
}

bool sendSensorValuesGSM_local(char* cfg_number, sensorVal_s* sensor_values)
{
  do {
    Serial.print("Sending sensor values = ");
    //serialGsm.listen();
    clearGSMBuffer();
    //serialGsm.flush();;

    serialGsm.println("AT");  //Once the handshake test is successful, it will back to OK
    delay(500);
    clearGSMBuffer();

    serialGsm.println(cfg_number);
    delay(500);

    /*String number_send_response = "ERROR";
    if (serialGsm.available()) {
      String full = serialGsm.readString();
      int index_of_nl = full.indexOf('\n');
      number_send_response = full.substring(abs(index_of_nl) + 1, abs(index_of_nl) + 1 + 5);  //get 5 characters after the NL
    }
    if (String("ERROR") == number_send_response) {
      //strings are equal. ERROR
      Serial.println("FAILED at CFG number");
      return false;
    }*/

    serialGsm.print("Temp: ");
    serialGsm.print(sensor_values->temperature, 2);  
    serialGsm.print(" C\nHum: ");
    serialGsm.print(sensor_values->humidity, 2);  
    serialGsm.print(" %\nDoor 1: ");
    serialGsm.print((int)sensor_values->door_1);  
    serialGsm.print("\nDoor 2: ");
    serialGsm.print((int)sensor_values->door_2);  
    serialGsm.print("\nFire: ");
    serialGsm.print((int)sensor_values->fire);  
    serialGsm.print("\nWater: ");
    serialGsm.print((int)sensor_values->water);  
    serialGsm.print("\nBat: ");
    serialGsm.print(sensor_values->battery);  
    serialGsm.print(" %\nAC In: ");
    serialGsm.print((int)sensor_values->ac_in);  
    serialGsm.print("\nAC Pow: ");
    serialGsm.print(sensor_values->ac_power);  
    serialGsm.print(" W\nUPS In: ");
    serialGsm.print((int)sensor_values->ups_in);  
    serialGsm.print("\nUPS Pow: ");
    serialGsm.print(sensor_values->ups_power);  
    serialGsm.print(" W");

    clearGSMBuffer();

    serialGsm.write(26);  //do not change. it is the Ctrl+Z in the ascii table

    delay(10000);  //wait till output get cleared

    //serialGsm.listen();
    clearGSMBuffer();
    //serialGsm.flush();;

    /*String send_response = "ERROR";
    if (serialGsm.available()) {
      String full = serialGsm.readString();

      //Serial.print(F("Income full: "));
      //Serial.println(full);

      int index_of_plus = full.indexOf('+');
      send_response = full.substring(abs(index_of_plus), abs(index_of_plus) + 5);  //get 5 characters including +
    }
    if (String("+CMGS") != send_response) {
      //strings are not equal. ERROR
      Serial.println("FAILED at +CMGS");
      return false;
    }*/
    Serial.println("SUCCESS!!!");

  } while (0);
  return true;
}

void sendSetConfirmation(String string_param, String phone_number)
{
  char* cfg_number; 
  cfg_number = malloc(23 * sizeof(char));
  strcpy(cfg_number, "AT+CMGS=\"");
  strcat(cfg_number, phone_number.c_str());
  strcat(cfg_number, "\"\0");

  Serial.print("Sending set confirmation = ");
  //serialGsm.listen();
  clearGSMBuffer();
  //serialGsm.flush();;
  
  serialGsm.println("AT");  //Once the handshake test is successful, it will back to OK
  delay(500);
  clearGSMBuffer();

  serialGsm.println(cfg_number);
  delay(500);

  serialGsm.print("Set: ");
  serialGsm.print(string_param);
  
  clearGSMBuffer();

  serialGsm.write(26);  //do not change. it is the Ctrl+Z in the ascii table

  delay(10000);  //wait till output get cleared
  //serialGsm.listen();
  clearGSMBuffer();
  //serialGsm.flush();;

  Serial.println("SUCCESS!!!");
}

#endif