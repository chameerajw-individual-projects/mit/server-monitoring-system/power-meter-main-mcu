#include "Arduino.h"
#ifndef IO_EXPANDER_H
#define IO_EXPANDER_H

#include "common_def.h"
#include "PCF8575.h"

#define RELAY_PIN A2

void resetUPS(void);

const uint8_t PCF8575_ADDR = 0x21;

const uint8_t LED_ARRAY_SIZE = 8;
uint8_t LED_array_prev[LED_ARRAY_SIZE] = { GREEN };  //Initialize this with GREEN. Original array will be RED. calling the switchLED function at init will light all the LEDs to RED.
uint8_t LED_array[LED_ARRAY_SIZE] = { RED, RED, RED, RED, RED, RED, RED, RED };

PCF8575 LED_IO(PCF8575_ADDR);

enum IO_port_idx {
  TEMPERATURE_GREEN_IDX = 0,
  HUMIDITY_GREEN_IDX = 1,
  DOOR_1_GREEN_IDX = 2,
  DOOR_2_GREEN_IDX = 3,
  FIRE_GREEN_IDX = 4,
  WATER_GREEN_IDX = 5,
  GSM_GREEN_IDX = 6,
  BATTERY_GREEN_IDX = 7,

  TEMPERATURE_RED_IDX = 15,
  HUMIDITY_RED_IDX = 14,
  DOOR_1_RED_IDX = 13,
  DOOR_2_RED_IDX = 12,
  FIRE_RED_IDX = 11,
  WATER_RED_IDX = 10,
  GSM_RED_IDX = 9,
  BATTERY_RED_IDX = 8
};

void IOExpanderInit(void);
void setLEDColors(sensorVal_s* sensor_val, sensorThreshold_s* threshold) ;
void switchLED(void);

void IOExpanderInit(void) {
#ifdef DEBUG_IO_EXPANDER
  Serial.print("[DEBUG]\tPCF8575_LIB_VERSION:\t");
  Serial.println(PCF8575_LIB_VERSION);
#endif
  LED_IO.begin();
  delay(500);
  switchLED();  //To set all GREEN

  pinMode(RELAY_PIN, OUTPUT);
}

void setLEDColors(sensorVal_s* sensor_val, sensorThreshold_s* threshold) {
  /************************ TEMPERATURE**************************/
  if ((sensor_val->temperature > 0.00) && (sensor_val->temperature <= threshold->temperature_thresh)) { //if sensor is OOD, 0
    LED_array[TEMPERATURE_IDX] = GREEN;
  } else {
    LED_array[TEMPERATURE_IDX] = RED;
  }

  /************************ HUMIDITY **************************/
  if ((sensor_val->humidity > 0.00) && (sensor_val->humidity <= threshold->humidity_thresh)) { //if sensor is OOD, 0
    LED_array[HUMIDITY_IDX] = GREEN;
  } else {
    LED_array[HUMIDITY_IDX] = RED;
  }

  /************************ DOOR_1 **************************/
  if (sensor_val->door_1 == true) {
    LED_array[DOOR_1_IDX] = RED;
  } else {
    LED_array[DOOR_1_IDX] = GREEN;
  }

  /************************ DOOR_2 **************************/
  if (sensor_val->door_2 == true) {
    LED_array[DOOR_2_IDX] = RED;
  } else {
    LED_array[DOOR_2_IDX] = GREEN;
  }

  /************************ FIRE **************************/
  if (sensor_val->fire == true) {
    LED_array[FIRE_IDX] = RED;
  } else {
    LED_array[FIRE_IDX] = GREEN;
  }

  /************************ WATER **************************/
  if (sensor_val->water == true) {
    LED_array[WATER_IDX] = RED;
  } else {
    LED_array[WATER_IDX] = GREEN;
  }

  /************************ GSM **************************/
  if (sensor_val->gsm == true) {
    LED_array[GSM_IDX] = RED;
  } else {
    LED_array[GSM_IDX] = GREEN;
  }

  /************************ BATTERY **************************/
  if (sensor_val->battery <= threshold->battery_thresh) {
    LED_array[BATTERY_IDX] = RED;
  } else {
    LED_array[BATTERY_IDX] = GREEN;
  }

  switchLED();
}

void switchLED(void) {
  /************************ TEMPERATURE**************************/
  if (LED_array_prev[TEMPERATURE_IDX] != LED_array[TEMPERATURE_IDX]) {
    LED_IO.write(TEMPERATURE_GREEN_IDX, LED_array[TEMPERATURE_IDX]);
    LED_IO.write(TEMPERATURE_RED_IDX, !(LED_array[TEMPERATURE_IDX]));

    LED_array_prev[TEMPERATURE_IDX] = LED_array[TEMPERATURE_IDX];
  }

  /************************ HUMIDITY **************************/
  if (LED_array_prev[HUMIDITY_IDX] != LED_array[HUMIDITY_IDX]) {
    LED_IO.write(HUMIDITY_GREEN_IDX, LED_array[HUMIDITY_IDX]);
    LED_IO.write(HUMIDITY_RED_IDX, !(LED_array[HUMIDITY_IDX]));

    LED_array_prev[HUMIDITY_IDX] = LED_array[HUMIDITY_IDX];
  }

  /************************ DOOR_1 **************************/
  if (LED_array_prev[DOOR_1_IDX] != LED_array[DOOR_1_IDX]) {
    LED_IO.write(DOOR_1_GREEN_IDX, LED_array[DOOR_1_IDX]);
    LED_IO.write(DOOR_1_RED_IDX, !(LED_array[DOOR_1_IDX]));

    LED_array_prev[DOOR_1_IDX] = LED_array[DOOR_1_IDX];
  }

  /************************ DOOR_2 **************************/
  if (LED_array_prev[DOOR_2_IDX] != LED_array[DOOR_2_IDX]) {
    LED_IO.write(DOOR_2_GREEN_IDX, LED_array[DOOR_2_IDX]);
    LED_IO.write(DOOR_2_RED_IDX, !(LED_array[DOOR_2_IDX]));

    LED_array_prev[DOOR_2_IDX] = LED_array[DOOR_2_IDX];
  }

  /************************ FIRE **************************/
  if (LED_array_prev[FIRE_IDX] != LED_array[FIRE_IDX]) {
    LED_IO.write(FIRE_GREEN_IDX, LED_array[FIRE_IDX]);
    LED_IO.write(FIRE_RED_IDX, !(LED_array[FIRE_IDX]));

    LED_array_prev[FIRE_IDX] = LED_array[FIRE_IDX];
  }

  /************************ WATER **************************/
  if (LED_array_prev[WATER_IDX] != LED_array[WATER_IDX]) {
    LED_IO.write(WATER_GREEN_IDX, LED_array[WATER_IDX]);
    LED_IO.write(WATER_RED_IDX, !(LED_array[WATER_IDX]));

    LED_array_prev[WATER_IDX] = LED_array[WATER_IDX];
  }

  /************************ GSM **************************/
  if (LED_array_prev[GSM_IDX] != LED_array[GSM_IDX]) {
    LED_IO.write(GSM_GREEN_IDX, LED_array[GSM_IDX]);
    LED_IO.write(GSM_RED_IDX, !(LED_array[GSM_IDX]));

    LED_array_prev[GSM_IDX] = LED_array[GSM_IDX];
  }

  /************************ BATTERY **************************/
  if (LED_array_prev[BATTERY_IDX] != LED_array[BATTERY_IDX]) {
    LED_IO.write(BATTERY_GREEN_IDX, LED_array[BATTERY_IDX]);
    LED_IO.write(BATTERY_RED_IDX, !(LED_array[BATTERY_IDX]));

    LED_array_prev[BATTERY_IDX] = LED_array[BATTERY_IDX];
  }
}

void resetUPS(void)
{
  digitalWrite(RELAY_PIN, HIGH);
  delay(1000);
  digitalWrite(RELAY_PIN, LOW);
}

#endif