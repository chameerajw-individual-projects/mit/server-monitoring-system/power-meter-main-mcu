#include "Arduino.h"
#include <stdint.h>
#include "HardwareSerial.h"
#ifndef EEPROM_MANAGER_H
#define EEPROM_MANAGER_H

#include "common_def.h"
#include <EEPROM.h>

#define EEPROM_SIZE 1024

void clearEEPROM(void) {
  for (int i = 0; i < EEPROM_SIZE; i++) {
    EEPROM.write(i, 0x00);
    delay(10);
  }
  while (1) {
    Serial.println("!!!!!!!!!!!!!!!!!*********** EEPROM CLEARED **********!!!!!!!!!!!!!!!!");
    //Stuck when EEPROM cleared
  }
}

void loadThresholdsFromEEPROM(sensorThreshold_s* threshold) {
  EEPROM.get(TEMPERATURE_THRESH_ADDR, threshold->temperature_thresh);
  EEPROM.get(HUMIDITY_THRESH_ADDR, threshold->humidity_thresh);
  EEPROM.get(BATTERY_THRESH_ADDR, threshold->battery_thresh);
  EEPROM.get(AC_CURRENT_MID_LEVEL_ADDR, threshold->ac_current_mid_level);
  EEPROM.get(UPS_CURRENT_MID_LEVEL_ADDR, threshold->ups_current_mid_level);

#ifdef DEBUG_EEPROM
  Serial.print("threshold->temperature_thresh:\t");
  Serial.println(threshold->temperature_thresh, 4);

  Serial.print("threshold->humidity_thresh:\t");
  Serial.println(threshold->humidity_thresh, 4);

  Serial.print("threshold->battery_thresh\t");
  Serial.println(threshold->battery_thresh);

  Serial.print("threshold->ac_current_mid_level:\t");
  Serial.println(threshold->ac_current_mid_level);

  Serial.print("threshold->ups_current_mid_level:\t");
  Serial.println(threshold->ups_current_mid_level);
#endif
}

void saveThreshodsInEEPROM(thresholdType type, float value) {
#ifdef DEBUG_EEPROM
  Serial.print("[DEBUG]: THRESHOLD TYPE: ");
  Serial.print(type);
  Serial.print("\t THRESHOLD VALUE: ");
  Serial.print(value, 4);
#endif
  switch (type) {
    case TEMPERATURE_THRESH_TYPE:
      EEPROM.put(TEMPERATURE_THRESH_ADDR, value);
      break;
    case HUMIDITY_THRESH_TYPE:
      EEPROM.put(HUMIDITY_THRESH_ADDR, value);
      break;
    case BATTERY_THRESH_TYPE:
      EEPROM.put(BATTERY_THRESH_ADDR, (int)value);
      break;
    case AC_CURRENT_MID_LEVEL_TYPE:
      EEPROM.put(AC_CURRENT_MID_LEVEL_ADDR, (int)value);
      break;
    case UPS_CURRENT_MID_LEVEL_TYPE:
      EEPROM.put(UPS_CURRENT_MID_LEVEL_ADDR, (int)value);
      break;
    default:
      Serial.println("[ERROR]\tINVALID THRESHOLD TYPE");
      break;
  }
}


#endif