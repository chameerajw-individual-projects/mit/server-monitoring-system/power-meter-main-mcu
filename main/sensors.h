#ifndef SENSORS_H
#define SENSORS_H

#include "common_def.h"
#include "DHT.h"

//#define DHTTYPE DHT11
#define DHTTYPE DHT21

#define DHT_PIN 8
#define FIRE_PIN 9
#define DOOR_1_PIN 10
#define DOOR_2_PIN 11
#define WATER_PIN 12

#define UPS_CURRENT_PIN A0
#define AC_IN_PIN A1
#define AC_CURRENT_PIN A3
#define UPS_IN_PIN A6

#define BATTERY_IN_PIN A7

#define CURRENT_SAMPLE_COUNT 100
#define CURRENT_SAMPLE_TIME_PERIOD 200  //us
#define CURRENT_AVG_COUNT 5

#define BATTERY_SAMPLE_COUNT 10
#define BATTERY_SAMPLE_TIME_PERIOD 200  //us

#define BAT_MIN_VOLTAGE_CONSTRAIN (float)3.700
#define BAT_MAX_VOLTAGE_CONSTRAIN (float)4.100

float calcACCurrent(int is_voltage_present);
float calcUPSCurrent(int is_voltage_present);
int calcBatteryVoltage(void);

float ac_current_reading_per_amphere = (float)(106 / 5);
float ups_current_reading_per_amphere = (float)(106 / 5);

float ac_current_offset = 0.09;
float ups_current_offset = 0.09;

float ac_current_mid_level_local = 0;
float ups_current_mid_level_local = 0;

float battery_voltage_reading_per_volt = (float)(965 / 4.200);

int voltage_230_110 = 230;

DHT dht(DHT_PIN, DHTTYPE);

void sensorsInit(sensorThreshold_s* threshold) {
  dht.begin();
  pinMode(DOOR_1_PIN, INPUT);
  pinMode(DOOR_2_PIN, INPUT);
  pinMode(FIRE_PIN, INPUT);
  pinMode(WATER_PIN, INPUT);

  pinMode(UPS_CURRENT_PIN, INPUT);
  pinMode(AC_IN_PIN, INPUT);
  pinMode(AC_CURRENT_PIN, INPUT);
  pinMode(UPS_IN_PIN, INPUT);

  pinMode(BATTERY_IN_PIN, INPUT);

  ac_current_mid_level_local = threshold->ac_current_mid_level;
  ups_current_mid_level_local = threshold->ups_current_mid_level;
}

void readSensors(sensorVal_s* in_sensor) {
  in_sensor->temperature = dht.readTemperature(false);  //isFahrenheit = false
  in_sensor->humidity = dht.readHumidity();  

  if(isnan(in_sensor->temperature))
  {
    in_sensor->temperature = 0;
  }
  if(isnan(in_sensor->humidity))
  {
    in_sensor->humidity = 0;
  }

  in_sensor->door_1 = digitalRead(DOOR_1_PIN);
  in_sensor->door_2 = digitalRead(DOOR_2_PIN);
  in_sensor->fire = digitalRead(FIRE_PIN);
  in_sensor->water = digitalRead(WATER_PIN);

  in_sensor->battery = calcBatteryVoltage();

  in_sensor->ac_in = !digitalRead(AC_IN_PIN);
  if (analogRead(UPS_IN_PIN) > 800) {
    in_sensor->ups_in = false;
  }
  else{
    in_sensor->ups_in = true;
  }

  in_sensor->ac_current = calcACCurrent(in_sensor->ac_in);
  in_sensor->ac_power = (in_sensor->ac_current * voltage_230_110);

  in_sensor->ups_current = calcUPSCurrent(in_sensor->ups_in);
  in_sensor->ups_power = (in_sensor->ups_current * voltage_230_110);

#ifdef DEBUG_SENSOR
  Serial.println("***************** SENSOR VALUES ******************");

  Serial.print("in_sensor->temperature:\t");
  Serial.println(in_sensor->temperature, 4);

  Serial.print("in_sensor->humidity:\t");
  Serial.println(in_sensor->humidity);

  Serial.print("in_sensor->door_1:\t");
  Serial.println(in_sensor->door_1);

  Serial.print("in_sensor->door_2:\t");
  Serial.println(in_sensor->door_2);

  Serial.print("in_sensor->fire:\t");
  Serial.println(in_sensor->fire);

  Serial.print("in_sensor->water:\t");
  Serial.println(in_sensor->water);

  Serial.print("in_sensor->battery:\t");
  Serial.println(in_sensor->battery);

  Serial.print("in_sensor->ac_in:\t");
  Serial.println(in_sensor->ac_in);

  Serial.print("in_sensor->ups_in:\t");
  Serial.println(in_sensor->ups_in);

  Serial.print("in_sensor->ac_current:\t");
  Serial.println(in_sensor->ac_current, 4);

  Serial.print("in_sensor->ac_power:\t");
  Serial.println(in_sensor->ac_power, 4);

  Serial.print("in_sensor->ups_current:\t");
  Serial.println(in_sensor->ups_current, 4);

  Serial.print("in_sensor->ups_power:\t");
  Serial.println(in_sensor->ups_power, 4);
#endif
}

float calcACCurrent(int is_voltage_present) {

  if (is_voltage_present == 1) {
    float min_value_avg = 0;
    float max_value_avg = 0;

    for (int i = 0; i < CURRENT_AVG_COUNT; i++) {
      int min_val = 1023;
      int max_val = 0;
      int read_val;
      for (int j = 0; j < CURRENT_SAMPLE_COUNT; j++) {
        read_val = analogRead(AC_CURRENT_PIN);
        max_val = max(read_val, max_val);
        min_val = min(read_val, min_val);
        delayMicroseconds(CURRENT_SAMPLE_TIME_PERIOD);
      }
      min_value_avg += min_val;
      max_value_avg += max_val;
    }


    min_value_avg = min_value_avg / CURRENT_AVG_COUNT;
    max_value_avg = max_value_avg / CURRENT_AVG_COUNT;

    float current_val_read = (abs(max_value_avg - ac_current_mid_level_local) + abs(ac_current_mid_level_local - min_value_avg)) / 2;
  
    return max(0, (current_val_read / ac_current_reading_per_amphere) - ac_current_offset);
  } else {
    return 0;
  }
}

float calcUPSCurrent(int is_voltage_present) {

  if (is_voltage_present == 1) {
    int min_value_avg = 0;
    int max_value_avg = 0;

    for (int i = 0; i < CURRENT_AVG_COUNT; i++) {
      int min_val = 1023;
      int max_val = 0;
      int read_val;
      for (int j = 0; j < CURRENT_SAMPLE_COUNT; j++) {
        read_val = analogRead(UPS_CURRENT_PIN);
        max_val = max(read_val, max_val);
        min_val = min(read_val, min_val);
        delayMicroseconds(CURRENT_SAMPLE_TIME_PERIOD);
      }
      min_value_avg += min_val;
      max_value_avg += max_val;
    }

    min_value_avg = min_value_avg / CURRENT_AVG_COUNT;
    max_value_avg = max_value_avg / CURRENT_AVG_COUNT;

    float current_val_read = (abs(max_value_avg - ups_current_mid_level_local) + abs(ups_current_mid_level_local - min_value_avg)) / 2;
    return max(0, (current_val_read / ups_current_reading_per_amphere) - ups_current_offset);
  } else {
    return 0;
  }
}

void setACVoltage(bool is110)
{
  if(true == is110)
  {
    voltage_230_110 = 110;
  }
  else 
  {
    voltage_230_110 = 230;
  }
}

int calcBatteryVoltage(void) {
  float value_read = 0;
  for (int i = 0; i < BATTERY_SAMPLE_COUNT; i++) {
    value_read += analogRead(BATTERY_IN_PIN);
    delayMicroseconds(BATTERY_SAMPLE_TIME_PERIOD);
  }

  value_read = value_read / BATTERY_SAMPLE_COUNT;
  float battery_voltage = (value_read / battery_voltage_reading_per_volt);
  battery_voltage = constrain(battery_voltage, BAT_MIN_VOLTAGE_CONSTRAIN, BAT_MAX_VOLTAGE_CONSTRAIN);
  return (int)(((battery_voltage - BAT_MIN_VOLTAGE_CONSTRAIN)  / (BAT_MAX_VOLTAGE_CONSTRAIN - BAT_MIN_VOLTAGE_CONSTRAIN)) * 100);
}

#endif