//Processor: Atmega328p- 3v3, 8Mhz

#include "common_def.h"
#include "IO_expander.h"
#include "sensors.h"
#include "EEPROM_manager.h"
#include "GSM.h"
#include "ESP_com.h"
#include "test.h"

#define SEND_VALUES_BIAS_THRESHOLD 10
#define PUBLISH_INTERVAL_MSEC (unsigned long)(120000) //2 minutes
sensorVal_s g_sensor_values;
sensorVal_s g_sensor_values_prev;
sensorThreshold_s g_sensor_thresholds;
gsmParams_s g_gsm_param;
switchState_s g_switch_state;

unsigned long g_previous_sent_millis = 0;

uint8_t gsm_undefined_count = 0;

void setup() {
#ifdef DEBUG
  Serial.begin(115200);
#endif

  if(_SS_MAX_RX_BUFF != 256)
  {    
    Serial.print("SoftwareSerial _SS_MAX_RX_BUFF: ");
    Serial.println(_SS_MAX_RX_BUFF);
    Serial.println("_SS_MAX_RX_BUFF should be 256");
    while(1);
  }

  loadThresholdsFromEEPROM(&g_sensor_thresholds);
  IOExpanderInit();
  sensorsInit(&g_sensor_thresholds);
  
  ESPInit();
  //delay(10000);
  //serialESP.listen();
  while (serialESP.available()) {
    decodeESPSerial(&g_switch_state);
  }
  setACVoltage(g_switch_state.voltage_230_110); // 0: 230, 1: 110 

  GSMInit();
  ////serialGsm.listen();
}

void loop() {
  readSensors(&g_sensor_values);
  setLEDColors(&g_sensor_values, &g_sensor_thresholds);

  int calc_bias = calcBiasOutput(&g_sensor_values, &g_sensor_values_prev); 
  if((calc_bias >= SEND_VALUES_BIAS_THRESHOLD) || ((millis() - g_previous_sent_millis) > PUBLISH_INTERVAL_MSEC))
  {
    //serialESP.listen();
    sendSensorValuesESP(&g_sensor_values);
    delay(5000);
    if(getSentConfirmation() == false)
    {
      //try to send only twice
      //Serial.println("SENDING ATTEMPT 1: FAILED!!!");
      sendSensorValuesESP(&g_sensor_values);
    }
    ////serialGsm.listen();
    serialGsm.flush();
    
    g_sensor_values_prev = g_sensor_values;
    g_previous_sent_millis = millis();
    //Serial.println(calc_bias);
  } 

  // gsmParams_s g_gsm_param;
  // g_gsm_param.gsm_command = GSM_EMPTY;
  // g_gsm_param.float_param = 100.00;
  // g_gsm_param.string_param = "temporary String";
  // g_gsm_param.phone_number = "+9471661909";
  GSMGetMessageDecoded(&g_gsm_param);
  
  if(g_gsm_param.gsm_command == GSM_UNDEFINED)
  {
    gsm_undefined_count ++;
    if(gsm_undefined_count > 1)
    {
      gsm_undefined_count = 0;
      GSMInit();
      ////serialGsm.listen();
    }
  }

  if(g_gsm_param.gsm_command != GSM_EMPTY)
  {
    processGSMCommand(g_gsm_param);
  }  

  delay(500);
}

int calcBiasOutput(sensorVal_s* sensor_val, sensorVal_s* sensor_val_pre)
{  
  int output_val = 0;
  float difference = 0;

  difference = abs(sensor_val_pre->temperature - sensor_val->temperature);
  output_val += (int)(difference * TEMPERATURE_BIAS);

  difference = abs(sensor_val_pre->humidity - sensor_val->humidity);
  output_val += (int)(difference * HUMIDITY_BIAS);

  difference = abs(sensor_val_pre->door_1 - sensor_val->door_1);
  output_val += (int)(difference * DOOR_1_BIAS);

  difference = abs(sensor_val_pre->door_2 - sensor_val->door_2);
  output_val += (int)(difference * DOOR_2_BIAS);

  difference = abs(sensor_val_pre->fire - sensor_val->fire);
  output_val += (int)(difference * FIRE_BIAS);

  difference = abs(sensor_val_pre->water - sensor_val->water);
  output_val += (int)(difference * WATER_BIAS);

  difference = abs(sensor_val_pre->gsm - sensor_val->gsm);
  output_val += (int)(difference * GSM_BIAS);

  difference = abs(sensor_val_pre->battery - sensor_val->battery);
  output_val += (int)(difference * BATTERY_BIAS);

  difference = abs(sensor_val_pre->ac_in - sensor_val->ac_in);
  output_val += (int)(difference * AC_IN_BIAS);

  difference = abs(sensor_val_pre->ups_in - sensor_val->ups_in);
  output_val += (int)(difference * UPS_IN_BIAS);

  //difference = abs(sensor_val_pre->ac_current - sensor_val->ac_current);
  //output_val += (int)(difference * AC_CURRENT_BIAS);
  //difference = abs(sensor_val_pre->ups_current - sensor_val->ups_current);
  //output_val += (int)(difference * UPS_CURRENT_BIAS);

  difference = abs(sensor_val_pre->ac_power - sensor_val->ac_power);
  output_val += (int)(difference * AC_POWER_BIAS);

  difference = abs(sensor_val_pre->ups_power - sensor_val->ups_power);
  output_val += (int)(difference * UPS_POWER_BIAS);

  return output_val;
}

void processGSMCommand(gsmParams_s gsmParams) {
  switch (gsmParams.gsm_command) {

    /************************ TEMPERATURE **************************/
    case GSM_SET_TEMPERATURE_THRESHOLD:
      if (gsmParams.float_param > 0) {
        g_sensor_thresholds.temperature_thresh = gsmParams.float_param;
        saveThreshodsInEEPROM(TEMPERATURE_THRESH_TYPE, g_sensor_thresholds.temperature_thresh); 
        sendSetConfirmation(String(gsmParams.float_param), gsmParams.phone_number);
      } else {
        Serial.println(F("[ERROR]: Invalid GSM param!"));
      }
      break;

    /************************ HUMIDITY **************************/
    case GSM_SET_HUMIDITY_THRESHOLD:
      if (gsmParams.float_param > 0) {
        g_sensor_thresholds.humidity_thresh = gsmParams.float_param;
        saveThreshodsInEEPROM(HUMIDITY_THRESH_TYPE, g_sensor_thresholds.humidity_thresh); 
        sendSetConfirmation(String(gsmParams.float_param), gsmParams.phone_number);
      } else {
        Serial.println(F("[ERROR]: Invalid GSM param!"));
      }
      break;

    /************************ BATTERY **************************/
    case GSM_SET_BATTERY_THRESHOLD:
      if (gsmParams.float_param > 0) {
        g_sensor_thresholds.battery_thresh = (int)(gsmParams.float_param); //cast into Int
        saveThreshodsInEEPROM(BATTERY_THRESH_TYPE, g_sensor_thresholds.battery_thresh); 
        sendSetConfirmation(String(gsmParams.float_param), gsmParams.phone_number);
      } else {
        Serial.println(F("[ERROR]: Invalid GSM param!"));
      }
      break;

    /************************ UPS **************************/
    case GSM_RESET_UPS:
      resetUPS();
      sendSetConfirmation("Reset UPS", gsmParams.phone_number);
      break;

    /************************ DASHBOARD **************************/
    case GSM_SET_DASHBOARD_FREQ:
      if (gsmParams.float_param > 0) {
        setDashboardFrequency(gsmParams.float_param);
        sendSetConfirmation(String(gsmParams.float_param), gsmParams.phone_number);
      } else {
        Serial.println(F("[ERROR]: Invalid GSM param!"));
      }
      break;

    /************************ WiFi **************************/
    case GSM_SET_WIFI:
      setWiFiCredentials(gsmParams.string_param);
      sendSetConfirmation(gsmParams.string_param, gsmParams.phone_number);
      break;

    /************************ THRESHOLDS **************************/
    case GSM_GET_THRESHOLD:
      sendThresholdsGSM(&g_sensor_thresholds, gsmParams.phone_number);
      break;

    /************************ SENSOR VALUES **************************/
    case GSM_GET_SENSOR_STATUS:
      sendSensorValuesGSM(&g_sensor_values, gsmParams.phone_number);
      break;

    case GSM_REINIT:
      GSMInit();
      ////serialGsm.listen();
      sendSetConfirmation("Reinited", gsmParams.phone_number);
      break;

    case GSM_UNDEFINED:
      sendSetConfirmation("UNDEFINED!", gsmParams.phone_number);
      break;


    default:
      break;
  }
}