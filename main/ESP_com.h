#ifndef ESP_COM_H
#define ESP_COM_H

#include "common_def.h"

#define ESP32_RX 6  //sim800 TX pin to pin 2. Arduino RX
#define ESP32_TX 5  //sim800 RX pin to pin 3. Arduino TX
#define ESP32_RST 7

#define MAX_GSM_INIT_ATTEMPTS 5

//SoftwareSerial serialESP(ESP32_RX, ESP32_TX);  // RX, TX
#define serialESP Serial3

void resetESP(void);

void setWiFiCredentials(String credentials) {
  serialESP.print('z');
  serialESP.println(credentials);
#ifdef DEBUG_ESP
  Serial.print("[DEBUG]\tWiFi credentials: ");
  Serial.println(credentials);
#endif
  delay(2000);
}

void setDashboardFrequency(float frequency) {
}

void ESPInit(void)
{  
  pinMode(ESP32_RST, OUTPUT);
  digitalWrite(ESP32_RST, HIGH);
  resetESP();
  serialESP.begin(9600);
  //serialESP.listen();
}

void resetESP(void)
{
  digitalWrite(ESP32_RST, LOW);
  delay(1000);
  digitalWrite(ESP32_RST, HIGH);
  delay(1000);
}

void sendSensorValuesESP(sensorVal_s *sensor_values)
{
  char esp_message[90];
  sprintf(esp_message, "t%dh%dx%dy%df%dw%db%da%dp%du%do%ds",
          (int)(sensor_values->temperature * 100),
          (int)(sensor_values->humidity * 100),
          (int)sensor_values->door_1,
          (int)sensor_values->door_2,
          (int)(sensor_values->fire),
          (int)sensor_values->water,
          (int)(sensor_values->battery),
          (int)sensor_values->ac_in,
          (int)(sensor_values->ac_power),
          (int)sensor_values->ups_in,
          (int)(sensor_values->ups_power));
  //final 's' is for sending data
#ifdef DEBUG_ESP
  Serial.print("[DEBUG]\tsendSensorValuesESP Message: ");
  Serial.println(esp_message);
#endif

  serialESP.println(esp_message);
  delay(2000);
}

void decodeESPSerial(switchState_s* switch_state) {
  char command = serialESP.read();
  switch (command) {
    /******************** VOLTAGE 230 110 ****************/
    case 'x':
      switch_state->voltage_230_110 = (bool)serialESP.parseInt();
#ifdef DEBUG_ESP_DECODER
      Serial.print("switch_state->voltage_230_110: ");
      Serial.println(switch_state->voltage_230_110);
#endif
      break;

    /******************** DIP 1 ****************/
    case 'a':
      switch_state->dip_1 = (bool)serialESP.parseInt();
#ifdef DEBUG_ESP_DECODER
      Serial.print("switch_state->dip_1: ");
      Serial.println(switch_state->dip_1);
#endif
      break;

    /******************** DIP 2 ****************/
    case 'b':
      switch_state->dip_2 = (bool)serialESP.parseInt();
#ifdef DEBUG_ESP_DECODER
      Serial.print("switch_state->dip_2: ");
      Serial.println(switch_state->dip_2);
#endif
      break;

    /******************** DIP 3 ****************/
    case 'c':
      switch_state->dip_3 = (bool)serialESP.parseInt();
#ifdef DEBUG_ESP_DECODER
      Serial.print("switch_state->dip_3: ");
      Serial.println(switch_state->dip_3);
#endif
      break;

    /******************** DIP 4 ****************/
    case 'd':
      switch_state->dip_4 = (bool)serialESP.parseInt();
#ifdef DEBUG_ESP_DECODER
      Serial.print("switch_state->dip_4: ");
      Serial.println(switch_state->dip_4);
#endif
      break;

  }
}

bool getSentConfirmation(void)
{
  if(serialESP.available())
  {
    char data = serialESP.read();
    if(data == 'c')
    {
      serialESP.flush();
      return true;
    }
  }
  return false;
}

#endif