#ifndef COMMON_DEF_H
#define COMMON_DEF_H

#include <stdint.h>
#include "Arduino.h"
#include "String.h"
#include "HardwareSerial.h"
#include <SoftwareSerial.h> 
//Copy and paste the ilbrary in the "libraries" folder to "C:\Users\chameeraw\AppData\Local\Arduino15\packages\arduino\hardware\avr\1.8.6\libraries" which has "#define _SS_MAX_RX_BUFF 256"

#define DEVICE_VERSION 0x01

#define DEBUG
//#define DEBUG_ESP
#define DEBUG_GSM
//#define DEBUG_EEPROM
//#define DEBUG_SENSOR
//#define DEBUG_IO_EXPANDER
//#define DEBUG_ESP_DECODER

#define TEMPERATURE_THRESH_ADDR (uint8_t)0x04
#define HUMIDITY_THRESH_ADDR (uint8_t)0x08
#define BATTERY_THRESH_ADDR (uint8_t)0x12
#define AC_CURRENT_MID_LEVEL_ADDR (uint8_t)0x16
#define UPS_CURRENT_MID_LEVEL_ADDR (uint8_t)0x20

enum LEDColor{
  GREEN = (uint8_t)0,
  RED = (uint8_t)1
};

enum thresholdType{
  TEMPERATURE_THRESH_TYPE,
  HUMIDITY_THRESH_TYPE,
  BATTERY_THRESH_TYPE,
  AC_CURRENT_MID_LEVEL_TYPE,
  UPS_CURRENT_MID_LEVEL_TYPE
};

enum sensorIdx {
  TEMPERATURE_IDX = 0,
  HUMIDITY_IDX = 1,
  DOOR_1_IDX = 2,
  DOOR_2_IDX = 3,
  FIRE_IDX = 4,
  WATER_IDX = 5,
  GSM_IDX = 6,
  BATTERY_IDX = 7,

  AC_CURRENT_IDX = 8,
  UPS_CURRENT_IDX = 9,
  AC_POWER_IDX = 10,
  UPS_POWER_IDX = 11
};

//Threshold is 10
enum sensorValBias {
  TEMPERATURE_BIAS = 5, //Difference > 2
  HUMIDITY_BIAS = 1, //Difference > 10
  DOOR_1_BIAS = 10, //Difference > 1 CRITICAL
  DOOR_2_BIAS = 10, //Difference > 1 CRITICAL
  FIRE_BIAS = 10, //Difference > 1 CRITICAL
  WATER_BIAS = 10, //Difference > 1 CRITICAL
  GSM_BIAS = 10, //Difference > 1 CRITICAL
  BATTERY_BIAS = 1, //Difference > 10

  AC_IN_BIAS = 10, //Difference > 1 CRITICAL
  UPS_IN_BIAS = 10, //Difference > 1 CRITICAL
  AC_CURRENT_BIAS = 20, //Difference > 0.5
  UPS_CURRENT_BIAS = 20, //Difference > 0.5
  AC_POWER_BIAS = 1, //Difference > 10
  UPS_POWER_BIAS = 1 //Difference > 10
};

enum gsmCommand{
  GSM_SET_TEMPERATURE_THRESHOLD,
  GSM_SET_HUMIDITY_THRESHOLD,
  GSM_SET_BATTERY_THRESHOLD,
  GSM_RESET_UPS,
  GSM_SET_DASHBOARD_FREQ,
  GSM_SET_WIFI,
  GSM_GET_THRESHOLD,
  GSM_GET_SENSOR_STATUS,
  GSM_REINIT,
  GSM_UNDEFINED,
  GSM_EMPTY
};

struct gsmParams_s{
  uint8_t gsm_command;
  float float_param;
  String string_param;
  String phone_number;
};

struct sensorVal_s {
  float temperature = 0.0;
  float humidity = 0.0;
  bool door_1 = false;
  bool door_2 = false;
  bool fire = false;
  bool water = false;
  bool gsm = false;
  uint8_t battery = 0;
  bool ac_in = false;
  bool ups_in = false;
  float ac_current = 0.0;
  float ups_current = 0.0;
  float ac_power = 0.0;
  float ups_power = 0.0;
};

struct sensorThreshold_s {
  float temperature_thresh = 30.00;
  float humidity_thresh = 80;
  uint8_t battery_thresh = 30;
  int ac_current_mid_level = 512;
  int ups_current_mid_level = 512;
};

struct switchState_s{
  bool voltage_230_110 = false;
  bool dip_1 = false;
  bool dip_2 = false;
  bool dip_3 = false;
  bool dip_4 = false;
};

#endif